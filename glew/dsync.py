'''
 Code used for extracting records from a specific source table. The pseudo logic:
  - Read snowflake and sourcedb connection info from secrtes manager
  - Connect to target datastore (Snowflake)
  - Check if the table exist in target datastore
  - If table does not exist perform initial data load/full data extract from source table
  - If table exist, identify max value of watermark column (ex: last updated timestamp).
    perform delta data extract from source table.
  - Store result in S3 data bucket.

Parameters:
  - SFLK_CONN_SECRETS : ARN of Secrets manager holding snowflake connection info
  - SOURCEDB_CONN_SECRETS : ARN of Secrets manager holding source db connection info
  - DSYNC_TABLE : Table to be migrated
  - TABLE_EXIST_QUERY : The query to be used to check if it exists in target data store
  - LATEST_WATERMARK_QUERY : The query to retrieve the max value of watermark col
  - INITIAL_LOAD_QUERY : The query used for full data extract
  - DELTA_LOAD_QUERY : The query used for delta load extract
  - S3_DATAD_DIR : The S3 folder where the data will be stored.
'''

import os ,sys, json, logging
import boto3
from botocore.exceptions import ClientError
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions #https://docs.aws.amazon.com/glue/latest/dg/aws-glue-api-crawler-pyspark-extensions-get-resolved-options.html
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from py4j.java_gateway import java_import
# import org.apache.spark.sql.SaveMode
# import org.apache.spark.sql.SaveMode
SNOWFLAKE_SOURCE_NAME = "net.snowflake.spark.snowflake"

MSG_FORMAT = '%(asctime)s %(levelname)s %(name)s: %(message)s'
DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
logging.basicConfig(format=MSG_FORMAT, datefmt=DATETIME_FORMAT)
logger = logging.getLogger('dsync')
logger.setLevel(logging.INFO)

## --------------------------------------------
args = getResolvedOptions(sys.argv, ['JOB_NAME', 'SFLK_CONN_SECRETS', 'SOURCEDB_CONN_SECRETS', 'DSYNC_TABLE', 
  'TABLE_EXIST_QUERY', 'LATEST_WATERMARK_QUERY', 'INITIAL_LOAD_QUERY', 'DELTA_LOAD_QUERY', 'S3_DATAD_DIR' ,'AWS_REGION'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)
java_import(spark._jvm, SNOWFLAKE_SOURCE_NAME)
# java_import(spark._jvm, 'org.apache.spark.sql.SaveMode')
#org.apache.spark.sql.SaveMode

MSG_FORMAT = '%(asctime)s %(levelname)s %(name)s: %(message)s'
DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
logging.basicConfig(format=MSG_FORMAT, datefmt=DATETIME_FORMAT)
logger = logging.getLogger('dsync')
logger.setLevel(logging.INFO)

## Ref : https://community.snowflake.com/s/article/How-To-Use-AWS-Glue-With-Snowflake
spark._jvm.net.snowflake.spark.snowflake.SnowflakeConnectorUtils.enablePushdownSession(spark._jvm.org.apache.spark.sql.SparkSession.builder().getOrCreate())

## --------------------------------------------
def get_secret_by_arn(secret_arn):
    text_secret_data = ''
    session = boto3.session.Session()
    client = session.client(service_name='secretsmanager', region_name=args['AWS_REGION'])

    try:
        logger.info('Retreiving secrets ...')
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_arn
        )
        logger.info('secrets response ' + str(get_secret_value_response))
    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            logger.info("The requested secret " + secret_arn + " was not found")
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            logger.info("The request was invalid due to:", e)
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            logger.info("The request had invalid params:", e)
    else:
        # Secrets Manager decrypts the secret value using the associated KMS CMK
        # Depending on whether the secret was a string or binary, only one of these fields will be populated
        if 'SecretString' in get_secret_value_response:
            text_secret_data = get_secret_value_response['SecretString']
        else:
            raise Exception("Only secrets stored as text/string is supported")

    return text_secret_data
 
logger.info('Retreiving snowflake connection info ...')
'''
SFLK_CONN_JS should be of format : 
{
      "SNOWSQL_ACCOUNT": "abc.us-east-1",
      "SNOWSQL_USER": "SOMEBODY",
      "SNOWSQL_PASSWORD": "abracadabra",
      "SNOWSQL_ROLE": "PUBLIC",
      "SNOWSQL_DATABASE": "DEMO_DB",
      "SNOWSQL_WAREHOUSE": "DEMO_WH"
    }
'''
sflk_conn_secrets = get_secret_by_arn(args['SFLK_CONN_SECRETS'])
sflk_conn_js = json.loads(sflk_conn_secrets)
snowflake_conn_info = {
    "sfURL" : sflk_conn_js['SNOWSQL_ACCOUNT'],
    "sfUser" : sflk_conn_js['SNOWSQL_USER'],
    "sfPassword" : sflk_conn_js['SNOWSQL_PASSWORD'],
    "sfDatabase" : sflk_conn_js['SNOWSQL_DATABASE'],
    "sfRole" : sflk_conn_js['SNOWSQL_ROLE'],
    "sfWarehouse" : sflk_conn_js['SNOWSQL_WAREHOUSE']
}

# Retreive source db conn info
sourcedb_conn_secrets = get_secret_by_arn(args['SOURCEDB_CONN_SECRETS'])
sourcedb_conn_js = json.loads(sourcedb_conn_secrets)

logger.info('SourceDB  : ' + str(sourcedb_conn_js))    
## --------------------------------------------
## Validate if the table to be loaded in Snowflake exists or not.

logger.info('Validating if table {} exist in target ...'.format(args['DSYNC_TABLE']))
target_table_ds_df = (spark.read
    .format(SNOWFLAKE_SOURCE_NAME)
    .options(**snowflake_conn_info)
    .option("query", args['TABLE_EXIST_QUERY'])
    .load())

does_table_exist_in_target = (target_table_ds_df.first()[0] != 0)
logger.info("Does table exist : " + str(does_table_exist_in_target))
delta_load = (does_table_exist_in_target == True)

query_toissue_on_sourcedb =  args['INITIAL_LOAD_QUERY']
## --------------------------------------------
## get the watermark value if the table exist
if (delta_load):
    logger.info("Peform delta load. Will need to be developed.")
    wm_df = (spark.read
        .format(SNOWFLAKE_SOURCE_NAME)
        .options(**snowflake_conn_info)
        .option("query", args['LATEST_WATERMARK_QUERY'])
        .load())
    watermark = wm_df.first()[0]
    logger.info("Last retreived watermark : " + str(watermark))
    query_toissue_on_sourcedb = args["DELTA_LOAD_QUERY"].format(str(watermark))
else:
    logger.info("Need to do a full load.")

logger.info(' Query against source db : ' + query_toissue_on_sourcedb)

# --------------------------------------------
# Connect to sql server

logger.info("Querying datasource ...")
source_table_df = (spark
                    .read.format("jdbc")
                    .option("url", sourcedb_conn_js['JDBC_URL'])
                    .option("driver", sourcedb_conn_js['DB_DRIVER'])
                    .option("user", sourcedb_conn_js['DB_USER'])
                    .option("password", sourcedb_conn_js['DB_PASSWORD'])
                    .option("dbtable", query_toissue_on_sourcedb)
                    .load())

## --------------------------------------------
## Store in S3
# logger.info("Writing to dir  : " + s3_bucket_root_data_dir)
# #source_table_df.write.parquet(s3_bucket_root_data_dir ,mode="overwrite")
# stored_df = glueContext.write_dynamic_frame.from_options(frame = store_df, connection_type = "s3", connection_options = {"path": s3_bucket_root_data_dir}, format = "parquet", transformation_ctx = "stored_df")

## --------------------------------------------
## Store in Snowflake
logger.info("Loading into Target {} ...".format(args['DSYNC_TABLE']))
#source_table_df.write.parquet(s3_bucket_root_data_dir ,mode="overwrite")
(source_table_df.write
.format(SNOWFLAKE_SOURCE_NAME)
.options(**snowflake_conn_info)
.option("dbtable", args['DSYNC_TABLE'])
.mode('append')
.save())
